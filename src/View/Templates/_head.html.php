<!DOCTYPE html>
<html lang="en">
    <head>
        <title>DWWM - Session</title>
        <meta charset="UTF-8">
        <meta name="description" content="Tutorial PHP MVC structure">
        <meta name="keywords" content="PHP,HTML,CSS,Bootstrap,MVC">
        <meta name="author" lang="fr" content="David RIEHL">
        <meta name="reply-to" content="david.riehl@gmail.com">
        <meta name="copyright" content="David RIEHL">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <!-- Bootstrap core CSS -->
        <link href="<?= $this->path; ?>/vendor/twbs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="<?= $this->path; ?>/src/View/Styles/navbar-top-fixed.css" rel="stylesheet">
    </head>
    <body>
