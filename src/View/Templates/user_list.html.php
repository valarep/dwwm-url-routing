<?php
use DWWM\Kernel\SessionManager;
?>
<?php require "_head.html.php"; ?>
<?php require "_nav.html.php"; ?>
        <main role="main" class="container">
            <h1>DWWM - Session</h1>
            <h2>Users</h2>
<?php if($this->isConnected): ?>        
<?php if (count(SessionManager::hasPrivileges("utilisateur/read", true)) == 1): ?>
            <table>
                <thead>
                    <th>Login</th>
                </thead>
                <tbody>
<?php if (count(SessionManager::hasPrivileges("utilisateur/update", true)) == 1): ?>
<?php foreach($this->users as $user): ?>        
                    <tr><td><a href="<?= $this->path; ?>/User/<?= $user->id; ?>"><?= $user->login; ?></a></td></tr>
<?php endforeach; ?>        
<?php else: ?>        
<?php foreach($this->users as $user): ?>        
                    <tr><td><?= $user->login; ?></td></tr>
<?php endforeach; ?>        
<?php endif; ?>        
                </tbody>
            </table>
<?php endif; ?>        
<?php endif; ?>
        </main>
<?php require "_body-end.html.php"; ?>